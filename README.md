# Kylie's exheres

Exheres for the [Exherbo Linux](https://www.exherbo.org) distribution.

## Contributions

Please direct all contributions to [GitLab](https://gitlab.exherbo.org/).

## Licensing

Things that go into Exherbo's official tree [must] [1] be licensed under
the GPLv2 license to be included in the tree. I don't like that license,
so I have dual-licensed the repository as both [GPLv2](LICENSE.GPLv2),
and [ISC](LICENSE.ISC).

In any case, check the header for the package; packages imported from
other repositories will probably just be GPLv2.

For any intents involving Exherbo's tree, use GPLv2. For anything else,
take your pick.

[1]: https://exherbo.org/docs/exheres-for-smarties.html#copyright_lines
