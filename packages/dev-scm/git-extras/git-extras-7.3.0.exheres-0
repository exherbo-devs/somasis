# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require bash-completion zsh-completion github [ user="tj" ]
require python [ blacklist=2 has_bin=true multibuild=false ]

SUMMARY="Useful additional git commands"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    test:
        dev-python/GitPython[>=3.1.43][python_abis:*(-)?]
        dev-python/pytest[>=8.1.1][python_abis:*(-)?]   [[ note = [ 8.1.2 actually ] ]]
        dev-python/testpath[python_abis:*(-)?]
    run:
        app-shells/bash
        dev-scm/git[>=2.17][-zsh-completion?] [[
            description = [ Cannot work with completion shipped in git ]
        ]]
        net-misc/curl
"

BASH_COMPLETIONS=( etc/bash_completion.sh )
ZSH_COMPLETIONS=( "etc/git-extras-completion.zsh _${PN}" )

DEFAULT_SRC_INSTALL_PARAMS=(
    PREFIX=/usr
    BINPREFIX=/usr/$(exhost --target)/bin
)

src_compile() {
    # runs install target by default
    :
}

src_test() {
    edo ${PYTHON} -m pytest -v
}

src_install() {
    default

    # Remove bash completions in wrong dir, install them later
    edo rm -r "${IMAGE}"/usr/etc

    bash-completion_src_install
    zsh-completion_src_install
}

